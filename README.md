## MusicBee SendToRecycleBin Plugin

__Features__:

- This is a simple [MusicBee](http://getmusicbee.com) plugin.
- Pressing a shorcut (Win + W), it sends the current playing song to the recycle bin, and plays next song. for sending the current
- You can set your own shortcut by going to Edit -> Preferences -> Hotkeys -> Playback: Delete Current Track and Play Next.

__How to add this plugin to MusicBee__:
1. Clone this repository somewhere on your computer.
2. Build the plugin binary called `mb_SendToRecycleBin.dll` by opening this repository in Microsoft Visual Studio.
3. Select Build -> Build solution from the menu.
4. In MusicBee, go to Edit -> Preferences -> Plugins, then click 'Add Plugin' at the top right corner.
5. Pick the file `musicbee-sendtorecyclebinplugin\bin\Debug\mb_SendToRecycleBin.dll`